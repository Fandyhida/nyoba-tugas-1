// Fandy Hidayat/Oseanografi 2020
// 12920042
// Class Circle dengan 3 instance
#include <iostream>
  
using namespace std;

// buat class Circle   
class Circle {
  public: // membuat semua orang dapat mengakses struktur class
    // buat 1 data member berupa radius
    float Radius; // variabelnya berupa angka desimal
    // buat 4 member function
    // mengatur fungsi untuk mendapatkan radius atau jari-jari lingkaran
    string setRadius(double Radius) {
     return "Masukkan Jari-jari = "; // menampilkan teks untuk memudahkan perintah
    }  
    // mengatur fungsi untuk menampilkan radius atau jari-jari lingkaran
    float getRadius(double Radius) {
     return Radius; // menampilkan nilai jari-jari 
    }
    // mengatur fungsi untuk mendapatkan luas lingkaran
    float getArea(double Radius) { 
     return Radius*Radius*3.14; // menghitung dan menampilkan nilai luas lingkaraan
    }
    // mengatur fungsi untuk mendapatkan keliling lingkaran
    float getCircumference(double Radius) {
     return 2*3.14*Radius; // menghitung dan menampilkan nilai keliling lingkaraan
    }
       
};
  
int main() // fungsi yang menghasilkan nilai interger
{
// buat objek dari class Circle (instansiasi)
  Circle pertama, kedua, ketiga;
  // set data member lagi
  float Radius;
  // tampilkan memberfunction untuk objek pertama
  cout << pertama.setRadius(Radius);
  cin >> Radius;
  cout << "nilai Radius = " << pertama.getRadius(Radius) << endl;
  cout << "nilai luas lingkaran = " << pertama.getArea(Radius) << endl;
  cout << "nilai keliling lingkaran = " << pertama.getCircumference(Radius) << endl;
  cout << "\n" << endl; // enter 1 baris 

  
  // tampilkan memberfunction untuk objek kedua
  cout << kedua.setRadius(Radius);
  cin >> Radius;
  cout << "nilai Radius = " << kedua.getRadius(Radius) << endl;
  cout << "nilai luas lingkaran = " << kedua.getArea(Radius) << endl;
  cout << "nilai keliling lingkaran = " << kedua.getCircumference(Radius) << endl;
  cout << "\n" << endl; // enter 1 baris
  
  // tampilkan memberfunction untuk objek ketiga
  cout << ketiga.setRadius(Radius);
  cin >> Radius;
  cout << "nilai Radius = " << ketiga.getRadius(Radius) << endl;
  cout << "nilai luas lingkaran = " << ketiga.getArea(Radius) << endl;
  cout << "nilai keliling lingkaran = " << ketiga.getCircumference(Radius) << endl;
  cout << "\n" << endl; // enter 1 baris
  return 0;

}